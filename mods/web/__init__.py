from utils import setup_logger
from mod import Mod


logger = setup_logger(__name__)


class WebMod(Mod):
    def __init__(self, context):
        super().__init__(context)
        logger.info('initialized')

    def deps(self):
        return set(('db', ))

    def start(self):
        super().start()
        logger.info('started')

    def stop(self):
        pass


Mod = WebMod
