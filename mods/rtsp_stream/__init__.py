import asyncio

from aioreactive.core import subscribe, AsyncStream, AsyncAnonymousObserver

from utils import setup_logger
from mod import Mod
from .stream_reader import StreamReader


logger = setup_logger(__name__)


class RtspMod(Mod):
    def __init__(self, context):
        super().__init__(context)
        logger.info('initialized')

    def stream_err_handler(self, cam):
        def callback(future):
            logger.warn(f'The stream from the {cam["name"]} was interrupted (exit_code: {future.result()})')
            self.context.loop.call_later(5, self.read_stream, cam)
        return callback

    def stream_handler(self, cam):
        stream = AsyncStream()
        sink = AsyncAnonymousObserver()

        async def mysink1(data):
            print('1', data)

        async def mysink2(data):
            print('2', data)

        async def callback(data):
            await subscribe(stream, AsyncAnonymousObserver(mysink1))
            await subscribe(stream, AsyncAnonymousObserver(mysink2))
            await stream.asend(data)
        return callback

    def read_stream(self, cam):
        logger.info(f'Starting a stream from the camera {cam["name"]}')
        sr = StreamReader(cam['rtsp_url']).exec(self.stream_handler(cam))
        future = asyncio.ensure_future(sr, loop=self.context.loop)
        future.add_done_callback(self.stream_err_handler(cam))

    def start(self):
        super().start()
        for cam in self.config['cams']:
            self.read_stream(cam)
        logger.info('started')

    def stop(self):
        pass


Mod = RtspMod
