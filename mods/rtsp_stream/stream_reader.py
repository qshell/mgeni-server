import asyncio


class StreamReader:
    def __init__(self, rtsp_url, buf_len=4096):
        self.buf_len = buf_len
        self.cmd = f'ffmpeg -i "{rtsp_url}" -c copy -movflags frag_keyframe+empty_moov -f mp4 -'

    async def exec(self, stdout_callback, stderr_callback=None):
        process = await asyncio.create_subprocess_shell(
            self.cmd,
            stdout=asyncio.subprocess.PIPE,
            stderr=asyncio.subprocess.PIPE
        )
        await asyncio.wait([
            self._read_stream(process.stdout, stdout_callback),
            self._read_stream(process.stderr, stderr_callback)
        ])
        return await process.wait()

    async def _read_stream(self, stream, callback):
        while True:
            buf = await stream.read(self.buf_len)
            if buf and callback:
                await callback(buf)
            else:
                break
