from utils import setup_logger
from mod import Mod


logger = setup_logger(__name__)


class TantosMod(Mod):
    def __init__(self, context):
        super().__init__(context)
        logger.info('initialized')

    def start(self):
        super().start()
        logger.info('started')

    def stop(self):
        pass


Mod = TantosMod
