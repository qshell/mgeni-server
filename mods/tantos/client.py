import asyncio
import struct
import json
import datetime


class TantosAction:
    LOGIN = (
        1000,
        lambda user, password: json.dumps({
            'EncryptType': 'MD5',
            'LoginType': 'DVRIP-Web',
            'PassWord': password,
            'UserName': user
        }).encode('utf8')
    )
    KEEP_ALIVE = (
        1006,
        lambda: json.dumps({}).encode('utf8')
    )
    SET_TIME = (
        1450,
        lambda session_id, time: json.dumps({
            'Name': 'OPTimeSetting',
            'SessionID': session_id,
            'OPTimeSetting': time
        }).encode('utf8')
    )
    GET_ENC_INFO = (
        1044,
        lambda session_id: json.dumps({
            'Name': 'Simplify.Encode',
            'SessionID': session_id
        }).encode('utf8')
    )
    SET_CAM_TITLE = (
        1046,
        lambda session_id: json.dumps({
            'Name': 'ChannelTitle',
            'SessionID': session_id,
            'ChannelTitle': ['FUCK']
        }).encode('utf8')
    )


class TantosClient:
    def __init__(self, loop, ip, user='admin', password='tlJwpbo6', port=34567):
        self.ip = ip
        self.loop = loop
        self.port = port
        self.packet_count = 0
        self.user = user
        self.password = password
        self.session_id = 0x00
        self.alive = True
        asyncio.ensure_future(self._open_connection())

    async def _open_connection(self):
        self.reader, self.writer = await asyncio.open_connection(self.ip, self.port, loop=self.loop)
        self.writer.write(self.build_packet(TantosAction.LOGIN, self.user, self.password))
        await asyncio.wait([self.main_reader_loop(), self.main_writer_loop()])
        self.writer.close()

    async def main_writer_loop(self):
        while self.alive:
            await asyncio.sleep(5)
            #self.writer.write(self.build_packet(TantosAction.KEEP_ALIVE))
            #self.writer.write(self.build_packet(
            #    TantosAction.SET_TIME,
            #    '0x%08X' % self.session_id,
            #    '{0:%Y-%m-%d %H:%M:%S}'.format(datetime.datetime.now()))
            #)
            self.writer.write(self.build_packet(
                TantosAction.SET_CAM_TITLE,
                '0x%08X' % self.session_id
            ))
            self.alive = False

    async def main_reader_loop(self):
        while self.alive:
            data_header = await self.reader.read(20)
            data = await self.handle_packet(data_header)
            print(data)
            if data['Ret'] != 100:
                self.alive = False

    def build_packet(self, tantos_action, *args):
        code, packet_builder = tantos_action
        data = packet_builder(*args)
        data += (chr(0x0a) + chr(0x00)).encode()
        head_flag = 0xff
        version = 0x00

        fmt_struct = 'BB2xII2xHI'

        packet = struct.pack(
            fmt_struct,
            head_flag,
            version,
            self.session_id,
            self.packet_count,
            code,
            len(data)
        )

        packet += data
        self.packet_count += 1
        print(packet)
        return packet

    async def handle_packet(self, data_header):
        _, _, self.session_id, _, _, data_len = struct.unpack('BB2xII2xHI', data_header)
        data = await self.reader.read(data_len)
        data = json.loads(data[:-2].decode('utf8'))
        return data


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    client = TantosClient(loop, '192.168.1.10')
    loop.run_forever()
