#!/usr/bin/env python3


import logging

import asyncio
from aiohttp import web

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils.executor import Executor
from aiogram.utils.context import task_factory as tb_task_factory

from db import DB


class TelegramBot:
    def __init__(self, token):
        logging.getLogger('aiogram').setLevel(logging.CRITICAL)
        bot = Bot(token=token)
        self.dp = Dispatcher(bot)

        @self.dp.message_handler(commands=['start'])
        async def register_user(message: types.Message):
            name = message.chat.first_name + ' ' + message.chat.last_name
            await DB().insert_user(message.chat.id, name)
            await message.reply(f'Hi, {name}!')

        @self.dp.message_handler()
        async def echo(message: types.Message):
            await bot.send_message(message.chat.id, message.text)

    def run(self, loop):
        exe = Executor(self.dp, skip_updates=True, loop=loop)
        loop.set_task_factory(tb_task_factory)
        loop.run_until_complete(exe._startup_polling())
        return exe.dispatcher.start_polling()


class WebServer:
    def __init__(self, host='localhost', port=8080):
        self.host = host
        self.port = port

    @staticmethod
    async def handler(request):
        name = request.match_info.get('name', 'Anonymous')
        text = 'Hello, ' + name
        return web.Response(text=text)

    async def run(self, loop):
        app = web.Application()
        app.router.add_routes([web.get('/{name}', WebServer.handler)])
        await loop.create_server(app.make_handler(), self.host, self.port)


def main():
    loop = asyncio.get_event_loop()
    DB(loop, 'tasks', sync=True)
    ws = WebServer(port=8888)
    tb = TelegramBot(token='509849109:AAFFtDFqn9CZMIPfOPcZhmE7skr8eu0UdAQ')
    asyncio.async(ws.run(loop))
    asyncio.async(tb.run(loop))
    loop.run_forever()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
