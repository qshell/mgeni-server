import sys
import logging


def setup_logger(name, verbosity_level=logging.INFO):
    name = name if name != '__main__' else 'mgeni'
    logger = logging.getLogger(name)
    logger.handlers = []
    logger.setLevel(verbosity_level)
    logger.propagate = False

    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s %(name)s: %(message)s',
        datefmt='%d-%m-%Y %H:%M:%S'
    )

    ch = logging.StreamHandler(sys.stdout)
    ch.setLevel(verbosity_level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    return logger


def singleton(cls):
    instances = {}

    def get_instance(*args, **kwargs):
        if cls not in instances:
            instances[cls] = cls(*args, **kwargs)
        return instances[cls]

    return get_instance
