#!/usr/bin/env python3

from os.path import dirname, join as path_join
from os import listdir
import asyncio

import utils
import config


VERSION = 0.1

logger = utils.setup_logger(__name__)
logger.info('MGeni v%s' % VERSION)


@utils.singleton
class Context(object):
    def __init__(self):
        self._data = {}

    def __getattr__(self, attr_name):
        return self._data.get(attr_name, None)

    def register(self, attr_name, obj):
        self._data[attr_name] = obj


def load_mods(mods):
    deps_tree = {mod_name: mod.deps() for mod_name, mod in mods.items()}
    while not all(mods.values()):
        dep_error = True
        for mod_name in deps_tree.keys():
            if mods[mod_name].started or deps_tree[mod_name]:
                continue
            for v in deps_tree.values():
                v.discard(mod_name)
            mods[mod_name].start()
            dep_error = False
        if dep_error:
            logger.error('Wrong dependency configuration')
            exit(1)


def init():
    import __main__
    mods = {}
    context = Context()
    context.register('loop', asyncio.get_event_loop())
    context.register('config', config.Config())
    logger.info('Initializing modules...')
    mods_dir = path_join(dirname(__main__.__file__), 'mods')
    root_mod = __import__('mods')
    for mod_name in filter(lambda m: not m.startswith('__'), listdir(mods_dir)):
        logger.info('Found module: %s' % mod_name)
        if not context.config.get_for(mod_name).get('enabled', False):
            logger.info('Module %s is disabled' % mod_name)
            continue
        __import__('mods.' + mod_name)
        mods[mod_name] = getattr(root_mod, mod_name).Mod(context)
    load_mods(mods)
    try:
        context.loop.run_forever()
    except KeyboardInterrupt:
        logger.info('stopping...')


if __name__ == '__main__':
    init()
