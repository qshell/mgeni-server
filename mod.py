from abc import ABCMeta, abstractmethod


class Mod(metaclass=ABCMeta):
    def __init__(self, context):
        self.name = self.__class__.__module__.split('.')[-1]
        self.config = context.config.get_for(self.name)
        self.context = context
        self._started = False

    def deps(self):
        return set()

    @abstractmethod
    def start(self):
        self._started = True

    @abstractmethod
    def stop(self):
        pass

    @property
    def started(self):
        return self._started

    def __bool__(self):
        return self.started
