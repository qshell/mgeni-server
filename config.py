import yaml

from utils import singleton


@singleton
class Config:
    def __init__(self):
        self.config = yaml.load(open('config.yaml'))

    def __getattr__(self, item):
        return self.config.get(item, None)

    def get_for(self, mod_name):
        return self.config.get(mod_name, None)
